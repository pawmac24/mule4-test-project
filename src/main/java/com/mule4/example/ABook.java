package com.mule4.example;

import java.io.Serializable;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "",
    propOrder = {"title", "author", "year", "price"}
)
@XmlRootElement(
    name = "ABook"
)
public class ABook implements Serializable {
	
	private static final long serialVersionUID = 1L;	
	
	@XmlElement(name = "title", required = true)
	protected String title;
	@XmlElement(name = "author", required = true)
	protected String author;
	@XmlElement(name = "year", required = true)
	protected int year;
	@XmlElement(name = "price", required = true)
	protected double price;

}
