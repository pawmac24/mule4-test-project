package com.mule4.example;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MyBook implements Serializable {
	
	private String title;
	private String author;
	private int year;
	private double price;

}
