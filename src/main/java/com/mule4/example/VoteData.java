package com.mule4.example;

import java.io.Serializable;
import java.math.BigDecimal;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class VoteData implements Serializable {

	private String firstName;
	private String lastName;
	private int age;
	private double price;

//	public VoteData(String firstName, String lastName, int age, double price) {
//		super();
//		this.firstName = firstName;
//		this.lastName = lastName;
//		this.age = age;
//		this.price = price;
//	}
}
