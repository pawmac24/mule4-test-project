package com.mule4.example;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(
    name = "",
    propOrder = {"aBook"}
)
@XmlRootElement(
    name = "Books"
)
public class Books implements Serializable {
	
	private static final long serialVersionUID = 1L;
	
	@XmlElement(name = "aBook", required = true)
	protected List<ABook> aBook;
	
    public List<ABook> getABook() {
        if (this.aBook == null) {
            this.aBook = new ArrayList<>();
        }
        return this.aBook;
    }

    public void setABook(List<ABook> value) {
        this.aBook = value;
    }
}
