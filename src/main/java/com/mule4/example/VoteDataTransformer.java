package com.mule4.example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.mule.runtime.api.message.Message;
import org.mule.runtime.api.streaming.bytes.CursorStream;
import org.mule.runtime.core.internal.streaming.bytes.ManagedCursorStreamProvider;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class VoteDataTransformer {

	// private final static Logger LOGGER =
	// Logger.getLogger(VoteDataTransformer.class.getName());

	public static List<VoteData> transformCVSToList(String voteDataStringList) {
		log.info("transformCVSToList {}", voteDataStringList);
		List<VoteData> voteDatas = new ArrayList<>();
		String[] lines = voteDataStringList.split("\\r?\\n");
		for (String line : lines) {
			String[] voteDataArray = line.split(";");
			voteDatas.add(new VoteData(voteDataArray[0], voteDataArray[1], new Integer(voteDataArray[2]),
					new Double(voteDataArray[3])));
		}
		return voteDatas;
	}
	
	public static List<VoteData> transformTXTToList(String voteDataStringList) {
		log.info("transformTXTToList {}", voteDataStringList);
		List<VoteData> voteDatas = new ArrayList<>();
		String[] lines = voteDataStringList.split("\\r?\\n");
		for (String line : lines) {
			String[] voteDataArray = line.split(" ");
			voteDatas.add(new VoteData(voteDataArray[0], voteDataArray[1], new Integer(voteDataArray[2]),
					new Double(voteDataArray[3])));
		}
		return voteDatas;
	}

	public static String transform(String stringLines) {
		log.info("transform {}", stringLines);
		return stringLines;
	}

	public static List<VoteData> transform(Object myObject) {
		log.info("transform");
		List<VoteData> voteDatas = new ArrayList<>();
		// String[] lines = voteDataStringList.split("\\r?\\n");
		// for (String line : lines) {
		// String[] voteDataArray = line.split(";");
		// voteDatas.add(new VoteData(voteDataArray[0], voteDataArray[1], new
		// Integer(voteDataArray[2]),new Double(voteDataArray[3])));
		// }
		return voteDatas;
	}

	public static List<VoteData> transform(Message message) {
		log.info("=== transform message payload = {}", message.getPayload());
		//log.info("=== transform message value = {}", message.getPayload().getValue());

		Object aPayload = message.getPayload().getValue();
		if (aPayload == null) {
			log.info("=== typedvalue is null");
		} else {
			log.info("=== typedvalue is OK");
			//ManagedCursorStreamProvider streamProvider = (ManagedCursorStreamProvider) message.getPayload().getValue();
			Class<? extends Object> class1 = aPayload.getClass();
			log.info("=== class1.getName() = {}", class1.getName()) ;
			
			if(aPayload instanceof ManagedCursorStreamProvider) {
				log.info("=== ManagedCursorStreamProvider === ") ;				
			}
//
//			try (CursorStream cursorStream = streamProvider.openCursor()) {
//				log.info("=== read start");
//				int data = cursorStream.read();
//				while (data != -1) {
//					data = cursorStream.read();
//					log.info("{}", ((char) data));
//				}
//			} catch (IOException e) {
//				log.error("DUPA {}", e);
//			}
//			log.info("=== read stop");
//
//			log.info("====== attributes = {}", message.getAttributes().getValue());
//			log.info("====== message type = {}", message.getAttributes().getDataType().getType());
		}
		List<VoteData> voteDatas = new ArrayList<>();
		return voteDatas;
	}

	public static int count(int size) {
		log.info("count");
		return size;
	}

	public static String doSth(Object object) {
		log.info("doSth");
		return "OK";
	}
}
