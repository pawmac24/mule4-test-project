package com.mule4.example;

public class TaxCalculator {
	
	private int price;
	private String description;
	
	public void init() {
		this.price = 0;
		this.description = "Nothing";
	}

	public TaxCalculator calculate(int price, String description) {
		this.price = 2 * price;
		this.description = "calculated - " + description;
		return this;
	}
}
