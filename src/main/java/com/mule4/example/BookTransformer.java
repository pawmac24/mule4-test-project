package com.mule4.example;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BookTransformer {
	
	public static List<MyBook> transform(String books) {
		log.info("transform String = {}", books);
		MyBook myBook = new MyBook("aaa", "bbb", 2015, 25.00);
		String a = myBook.getAuthor();
		
		ObjectMapper objectMapper = new ObjectMapper();
		List<MyBook> bookList = new ArrayList<>();
		try {
			bookList = objectMapper.readValue(books, new TypeReference<List<MyBook>>(){});
			log.info("Java = {}",Arrays.toString(bookList.toArray()));
		} catch (IOException e) {
			log.error(e.getMessage());
		}
		return bookList;
	}
	
	public static void transform(MyBook[] books) {
		log.info("transform MyBook[]");
	}
	
	public static String transform(List<MyBook> books) {
		log.info("transform List<MyBook> {}", books);
		for (MyBook myBook: books) {
			myBook.setAuthor(myBook.getAuthor() + "_conv");
			myBook.setTitle(myBook.getTitle() + "_conv");
		}
		
		ObjectMapper mapper = new ObjectMapper();
		String jsonString = "";
		try {
			jsonString = mapper.writeValueAsString(books);
		} catch (JsonProcessingException e) {
			log.error(e.getMessage());
		}
		return jsonString;
	}
	
	public static List<MyBook> modify(List<MyBook> books) {
		log.info("modify List<MyBook> {}", books);
		for (MyBook myBook: books) {
			myBook.setAuthor(myBook.getAuthor() + "_conv");
			myBook.setTitle(myBook.getTitle() + "_conv");
		}
		return books;
	}
	
	public static void mapTo(String myArray) {
		log.info("mapTo");
	}

}
