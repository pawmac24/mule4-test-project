## Enpoints:
```
http://localhost:8082/helloworld?language=Spanish
{
	"main": "abc",
	"price" : 25,
	"description" : "my description"
}
Content-Type : application/json

http://localhost:8082/complex
http://localhost:8082/jms

[
	{
		"title":"Tytuł 1",
		"author": "Autor 1",
		"year": "2005",
		"price": "30.00"
	},
	{
		"title":"Tytuł 2",
		"author": "Autor 2",
		"year": "2008",
		"price": "25.50"
	},
	{
		"title":"Tytuł 3",
		"author": "Autor 3",
		"year": "2007",
		"price": "27.00"
	}
]

Example DEMO-APP:
http://localhost:8080/demo-app/getDataFTP
http://localhost:8080/demo-app/putFile/bbb.txt
```

## Before run mule4-test-project:

1. Run Active MQ 5.15.9 :
```
http://127.0.0.1:8161/admin/
admin/admin
activemq start
```

2. Run demo-app springboot application
3. Run local ftp server on FILEZILLA

## Run tests - example calling:
```
mvn clean package -Dmule_env=test
mvn clean test -Dmunit.test=read-file-test-suite.xml
mvn clean test -Dmunit.test=read-file-test-suite.xml#read-txt-and-csv-file
mvn clean test -Dmunit.test=jms-send-test-suite.xml
mvn clean test -Dmunit.test=example-listener-test-suite.xml#call-post-listener-with-example-payload

```